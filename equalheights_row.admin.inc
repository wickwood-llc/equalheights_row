<?php

/**
 * Admin configuration form to configure the module settings.
 */
function equalheights_row_admin_config_form ($form, &$form_state) {
  $form['equalheights_row_pages'] = array(
    '#title' => t('Pages'),
    '#type' => 'textarea',
    '#description' => t('Enter the paths of pages where the plugin is to be loaded and acted upon. Specify pages by using their paths. Enter one path per line. The \'*\' character is a wildcard. Example paths are <em>blog</em> for the blog page and <em>blog/*</em> for every personal blog. &lt;front&gt; is the front page. Putting ~ character at beginning of a pattern will make it negation, meaning that path will be excluded. It will be helpful to exclude certain sub-pages to be excluded when parent pages are included.'),
    '#default_value' => variable_get('equalheights_row_pages', ''),
  );
  $form['equalheights_row_elements_and_conditions'] = array(
    '#title' => t('HTML elements and conditions'),
    '#type' => 'textarea',
    '#description' => t('Enter CSS selector for parent and children. Then provide the minimum width of page to make equal height. Separate these three values by |. You may enter one or more these sets, each of the set to be in separate line.'),
    '#default_value' => variable_get('equalheights_row_elements_and_conditions', ''),
  );
  $form['equalheights_row_events'] = array(
    '#title' => t('Events'),
    '#type' => 'textfield',
    '#description' => t('Enter jQuery event names. Separate each name by comma. Heights of columns will get recalculated on these events.'),
    '#default_value' => variable_get('equalheights_row_events', 'ready,resize'),
  );
  $form['equalheights_row_recalculation_delay'] = array(
    '#title' => t('Delay'),
    '#type' => 'textfield',
    '#description' => t('Delay in milliseconds. Height recalculation will be delayed by this time.'),
    '#default_value' => variable_get('equalheights_row_recalculation_delay', 250),
  );
  return system_settings_form($form);
}
