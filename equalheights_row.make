api = 2
core = 7.x

; Libraries
libraries[jquery.equalheights][directory_name] = jquery.equalheights
libraries[jquery.equalheights][download][type] = file
libraries[jquery.equalheights][download][url] = https://gitlab.com/wickwood-llc/jquery.equalheights/-/archive/master/jquery.equalheights-master.tar.gz
libraries[jquery.equalheights][type] = library
