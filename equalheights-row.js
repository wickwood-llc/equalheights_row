(function ($) {
  var equalheights_row_timeout;
  /**
   * Make equal height for items within .featured-display container.
   */
  var equalheights_row_perform_equalheight = function() {
    // We avoid frequent recalculation of heights using clearTimeout and setTimeout.
    clearTimeout(equalheights_row_timeout);
    equalheights_row_timeout = setTimeout(function(){
      var i;
      for (i = 0; i < Drupal.settings.equalheights_row.elements_and_conditions.length; i++) {
        var item = Drupal.settings.equalheights_row.elements_and_conditions[i];
        if ($(window).width() > item.min_width) {
          $(item.parent).each(function() {
            $(item.children, this).equalHeights();
          });
        }
        else {
          $(item.parent + ' ' + item.children).css({
            'height': 'auto',
          })
        }
      }
    }, Drupal.settings.equalheights_row.delay);
  };
  Drupal.behaviors.equalheightsRow = {
    attach: function (context, settings) {
      if (Drupal.settings.equalheights_row.events.indexOf("resize") > -1) {
        $(window).on('resize', function() {
          equalheights_row_perform_equalheight();
        });
      }
      $(document).on(Drupal.settings.equalheights_row.events.join(' '), function() {
        equalheights_row_perform_equalheight();
      });
    }
  };
}(jQuery));
